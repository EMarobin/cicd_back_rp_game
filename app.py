from flask import Flask

from routes.routes_hero import hero_app
from routes.routes_enemy import enemy_app
from routes.routes_item import item_app


app = Flask(__name__)
app.register_blueprint(hero_app)
app.register_blueprint(enemy_app)
app.register_blueprint(item_app)

if __name__ == '__main__':
    app.run()

