class Hero:
    def __init__(self, classChoice):
        self.classDef = 'NoClass',
        self.name = 'NoName',
        self.hp = 0,
        self.ATK = 0
        self.status = ''

    def setup_hero(self, classtype, name):
        self.classDef = classtype
        self.name = name
        match classtype:
            case 'warrior':
                self.hp = 12
                self.ATK = 5
            case 'archer':
                self.hp = 12
                self.ATK = 5
            case 'mage':
                self.hp = 9
                self.ATK = 5

    def modifHP(self, malus):
        self.hp = self.hp - malus

