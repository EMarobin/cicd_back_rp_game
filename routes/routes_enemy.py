from flask import Blueprint, request, jsonify
import json

enemy_app = Blueprint('enemy_app', __name__)


# Récupérer tous les ennemis
@enemy_app.route('/enemy/all', methods=['GET'])
def getall_enemy():
    with open('datas/enemy.json', 'r', encoding="utf-8") as json_file:
        data = json_file.read()
    return jsonify(json.loads(data)), 200

# Récupérer un ennemi
@enemy_app.route('/enemy/<string:className>', methods=['GET'])
def get_enemy(className):
    # Charger le fichier JSON contenant les données des ennemis
    with open('datas/enemy.json', 'r') as json_file:
        enemies_data = json.load(json_file)

    # Vérifier si la classe spécifiée existe dans les données des ennemis
    if className in enemies_data['enemy']:
        # Récupérer les données du serpent
        snake_data = enemies_data['enemy'][className]

        # Retourner les données du serpent en tant que réponse JSON
        return jsonify(snake_data), 200
    else:
        # Retourner une réponse d'erreur si la classe spécifiée n'existe pas
        return jsonify({'error': 'Ennemi introuvable'}), 404


# Mettre à jour un ennemi existant
@enemy_app.route('/enemy/upd/<string:className>', methods=['PUT'])
def update_enemy(className):
    damageReceived = request.json.get('dmg')
    with open('datas/enemy.json', 'r') as json_file:
        data = json.load(json_file)

    if className in data['enemy']:
        data['enemy'][className]["hp"] -= damageReceived

    json_file.close()

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/enemy.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return jsonify({"message": className + " modifié"}), 201


# Route pour réinitialiser les ennemis
@enemy_app.route('/enemy/clean', methods=['POST'])
def clean_enemy():
    with open('datas/enemy.json', 'r') as json_file:
        data = json.load(json_file)

    data["enemy"]["snake"]["hp"] = 5
    data["enemy"]["bandit"]["hp"] = 8

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/hero.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return jsonify(data)
