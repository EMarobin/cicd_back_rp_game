from flask import Blueprint, jsonify, request
import random
import json

hero_app = Blueprint('hero_app', __name__)


# Route pour récupérer le héros
@hero_app.route('/hero/get', methods=['GET'])
def get_hero():
    # Ouvrir le fichier JSON
    with open('datas/hero.json', 'r', encoding="utf-8") as json_file:
        data = json_file.read()
    return jsonify(json.loads(data)), 200


# Route pour créer un nouveau héros
@hero_app.route('/hero/init', methods=['POST'])
def init_hero():
    nameReceived = request.json.get('name')
    renameMethod = rename(nameReceived)
    # N'accepte pas les returns de fonction
    # return get_hero(), 201
    if renameMethod:
        return jsonify({"message": "Héro créé"}), 201
    else:
        return jsonify({"message": "Création impossible"}), 404


# Route pour mettre à jour le héros
@hero_app.route('/hero/upd', methods=['PUT'])
def update_hero():
    # calcul des stats
    damageReceived = request.json.get('dmg')
    heroClass = request.json.get('class')
    statusReceived = request.json.get('status')
    bonusDamage = request.json.get('AKTBonus')

    with open('datas/hero.json', 'r') as json_file:
        data = json.load(json_file)

    # Effectuer les modifications souhaitées sur les données
    data['hero'][heroClass]['stats']['hp'] = data['hero'][heroClass]['stats']['hp'] + damageReceived
    data['hero'][heroClass]['stats']['ATK'] += bonusDamage
    if statusReceived == 1:
        if randomStatus() == 10:
            data['hero'][heroClass]['status'] = 'Poison'

    # Fermer le fichier JSON en mode lecture
    json_file.close()

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/hero.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return jsonify({"message": "Héros modifié"}), 201


# Route pour supprimer le héros
@hero_app.route('/hero/clean', methods=['DELETE'])
def delete_hero():
    rename("")
    classReceived = request.json.get('class')
    resetStats()
    return jsonify({"message": "Héros supprimé"}), 201


def rename(nameSend):
    with open('datas/hero.json', 'r') as json_file:
        data = json.load(json_file)

    # Effectuer les modifications souhaitées sur les données
    data['hero']['warrior']['name'] = nameSend
    data['hero']['mage']['name'] = nameSend
    data['hero']['archer']['name'] = nameSend

    # Fermer le fichier JSON en mode lecture
    json_file.close()

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/hero.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return True


def resetStats():
    with open('datas/hero.json', 'r') as json_file:
        data = json.load(json_file)

    data['hero']['warrior']['stats']['hp'] = 12
    data['hero']['warrior']['stats']['ATK'] = 5
    data['hero']['warrior']['status'] = ""
    data['hero']['mage']['stats']['hp'] = 9
    data['hero']['mage']['stats']['ATK'] = 5
    data['hero']['mage']['status'] = ""
    data['hero']['archer']['stats']['hp'] = 12
    data['hero']['archer']['stats']['ATK'] = 5
    data['hero']['archer']['status'] = ""

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/hero.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()


def randomStatus():
    random_number = random.randint(1, 10)
    return random_number
