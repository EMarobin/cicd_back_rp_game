from flask import Blueprint, request, jsonify
import json

item_app = Blueprint('item_app', __name__)


# Récupérer tous les objets
@item_app.route('/item/get', methods=['GET'])
def get_item():
    with open('datas/item.json', 'r', encoding="utf-8") as json_file:
        data = json.load(json_file)
    return jsonify(data), 200


# Récupérer un objet
@item_app.route('/item/get/<string:className>', methods=['GET'])
def get_item_id(className):
    with open('datas/item.json', 'r', encoding="utf-8") as json_file:
        data = json.load(json_file)

    if className in data:
        item_data = data[className]
        return jsonify(item_data), 200

    return jsonify({"message": "Objet non trouvé"}), 404


# Mettre à jour un objet existant
@item_app.route('/item/upd/<string:className>', methods=['PUT'])
def update_item(className):
    with open('datas/item.json', 'r', encoding="utf-8") as json_file:
        data = json.load(json_file)

    if className in data:
        data[className]["name"] = request.json.get('name')
        data[className]["pts_item"] = request.json.get('pts_item')
        data[className]["pts"] = request.json.get('pts')

    json_file.close()

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/item.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return jsonify({"message": "Item " + className + " modifié"}), 201


# Route pour réinitialiser les objets
@item_app.route('/item/clean', methods=['POST'])
def clean_item():
    with open('datas/item.json') as json_file:
        data = json.load(json_file)

    data["warrior"]["name"] = "épée de la mort"
    data["warrior"]["pts_item"] = 2
    data["warrior"]["pts"] = 4
    data["mage"]["name"] = "baton du puissant sorcier"
    data["mage"]["pts_item"] = 2
    data["mage"]["pts"] = 4
    data["archer"]["name"] = "arc de l'arbre divin"
    data["archer"]["pts_item"] = 2
    data["archer"]["pts"] = 4

    json_file.close()

    # Ouvrir le fichier JSON en mode écriture
    with open('datas/item.json', 'w') as json_file:
        # Écrire les données mises à jour dans le fichier JSON
        json.dump(data, json_file, indent=4)

    # Fermer le fichier JSON en mode écriture
    json_file.close()

    return jsonify({"message": "Items nettoyés"}), 201
