import unittest
import json
from unittest import mock

from flask import Flask
from routes.routes_enemy import enemy_app


class EnemyAppTestCase(unittest.TestCase):

    data_json_enemy = {
        "enemy": {
            "snake": {
                "id": 1,
                "name": "snake",
                "hp": 5,
                "ATK": 2,
                "poison": 0.1
            },
            "bandit": {
                "id": 2,
                "name": "bandit",
                "hp": 8,
                "ATK": 3
            }
        }
    }

    def setUp(self):
        self.app = Flask(__name__)
        self.app.register_blueprint(enemy_app)

        self.client = self.app.test_client()

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_getall_enemy(self):
        response = self.client.get('/enemy/all')
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(data)
        self.assertEqual(data, {"enemy": {
                "snake": {
                    "id": 1,
                    "name": "snake",
                    "hp": 5,
                    "ATK": 2,
                    "poison": 0.1
                },
                "bandit": {
                    "id": 2,
                    "name": "bandit",
                    "hp": 8,
                    "ATK": 3
                }
            }
        })

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_get_enemy(self):
        className = 'snake'

        response = self.client.get(f'/enemy/{className}')
        data = response.get_json()

        expected_data = {
            "id": 1,
            "name": "snake",
            "hp": 5,
            "ATK": 2,
            "poison": 0.1
        }

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_get_enemy_not_found(self):
        className = 'dragon'

        response = self.client.get(f'/enemy/{className}')
        data = response.get_json()

        expected_data = {'error': 'Ennemi introuvable'}

        self.assertEqual(response.status_code, 404)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)




    def test_update_enemy(self):
        payload = {
            'dmg': 2
        }
        response = self.client.put('/enemy/upd/snake', json=payload)
        data = response.get_json()

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        # Ajoutez d'autres assertions pour vérifier le message de succès

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_update_enemy(self):
        className = 'snake'
        payload = {
            'dmg': 2
        }

        response = self.client.put(f'/enemy/upd/{className}', json=payload)
        data = response.get_json()

        expected_data = {"message": "snake modifié"}

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_update_enemy_not_found(self):
        className = 'dragon'
        payload = {
            'dmg': 2
        }

        response = self.client.put(f'/enemy/upd/{className}', json=payload)
        data = response.get_json()

        expected_data = {"message": "dragon modifié"}

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_enemy)))
    def test_clean_enemy(self):
        response = self.client.post('/enemy/clean')
        cleaned_data = response.get_json()

        expected_data = {
            "enemy": {
                "snake": {
                    "id": 1,
                    "name": "snake",
                    "hp": 5,
                    "ATK": 2,
                    "poison": 0.1
                },
                "bandit": {
                    "id": 2,
                    "name": "bandit",
                    "hp": 8,
                    "ATK": 3
                }
            }
        }

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(cleaned_data)
        self.assertEqual(cleaned_data, expected_data)

if __name__ == '__main__':
    unittest.main()
