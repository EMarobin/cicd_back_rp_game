import unittest
import json
from unittest import mock

from flask import Flask
from routes.routes_hero import hero_app


class HeroAppTestCase(unittest.TestCase):

    data_json_hero = {
        "hero": {
            "warrior": {
                "class": "Warrior",
                "name": None,
                "stats": {
                    "hp": 12,
                    "ATK": 5
                },
                "status": ""
            },
            "mage": {
                "class": "Mage",
                "name": None,
                "stats": {
                    "hp": 9,
                    "ATK": 5
                },
                "status": ""
            },
            "archer": {
                "class": "Archer",
                "name": None,
                "stats": {
                    "hp": 12,
                    "ATK": 5
                },
                "status": ""
            }
        }
    }

    def setUp(self):
        self.app = Flask(__name__)
        self.app.register_blueprint(hero_app)

        self.client = self.app.test_client()

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_hero)))
    def test_get_hero(self):
        response = self.client.get('/hero/get')
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(data)
        self.assertEqual(data, {"hero": {
            "warrior": {
                "class": "Warrior",
                "name": None,
                "stats": {
                    "hp": 12,
                    "ATK": 5
                },
                "status": ""
            },
            "mage": {
                "class": "Mage",
                "name": None,
                "stats": {
                    "hp": 9,
                    "ATK": 5
                },
                "status": ""
            },
            "archer": {
                "class": "Archer",
                "name": None,
                "stats": {
                    "hp": 12,
                    "ATK": 5
                },
                "status": ""
            }
        }})

    @mock.patch('routes.routes_hero.rename')
    def test_init_hero(self, mock_rename):
        payload = {
            'name': 'Gimli'
        }

        # Configurer le mock de la fonction "rename" pour toujours renvoyer True
        mock_rename.return_value = True

        response = self.client.post('/hero/init', json=payload)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, {"message": "Héro créé"})

    @mock.patch('routes.routes_hero.rename')
    def test_init_hero_false(self, mock_rename):
        payload = {
            'name': 'Gimli'
        }

        # Configurer le mock de la fonction "rename" pour toujours renvoyer True
        mock_rename.return_value = False

        response = self.client.post('/hero/init', json=payload)
        data = json.loads(response.data.decode('utf-8'))

        self.assertEqual(response.status_code, 404)
        self.assertIsNotNone(data)
        self.assertEqual(data, {"message": "Création impossible"})

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_hero)))
    def test_update_hero(self):

        payload = {
            'dmg': 2,
            'class': 'warrior',
            'status': 1,
            'AKTBonus': 2
        }
        data = {
            "hero": {
                "warrior": {
                    "class": "Warrior",
                    "name": "HeroName",
                    "stats": {
                        "hp": 12,
                        "ATK": 5
                    },
                    "status": ""
                },
                "mage": {
                    "class": "Mage",
                    "name": "HeroName",
                    "stats": {
                        "hp": 9,
                        "ATK": 5
                    },
                    "status": ""
                },
                "archer": {
                    "class": "Archer",
                    "name": "HeroName",
                    "stats": {
                        "hp": 12,
                        "ATK": 5
                    },
                    "status": ""
                }
            }
        }

        response = self.client.put('/hero/upd', json=payload)
        data = response.get_json()

        expected_data = {"message": "Héros modifié"}

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_hero)))
    @mock.patch('routes.routes_hero.rename')
    def test_delete_hero(self, mock_rename):

        payload = {
            'class': ''
        }

        mock_rename.return_value = True

        response = self.client.delete('/hero/clean', json=payload)
        data = response.get_json()

        expected_data = {"message": "Héros supprimé"}

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)


if __name__ == '__main__':
    unittest.main()
