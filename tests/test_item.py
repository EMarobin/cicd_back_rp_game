import unittest
import json
from unittest import mock

from flask import Flask
from routes.routes_item import item_app


class ItemAppTestCase(unittest.TestCase):

    data_json_item = {
        "warrior": {
            "name": "Épée de la mort",
            "pts_item": 2,
            "pts": 4
        },
        "mage": {
            "name": "Baton du puissant sorcier",
            "pts_item": 2,
            "pts": 4
        },
        "archer": {
            "name": "Arc de l'arbre divin",
            "pts_item": 2,
            "pts": 4
        }
    }

    def setUp(self):
        self.app = Flask(__name__)
        self.app.register_blueprint(item_app)
        self.client = self.app.test_client()

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_get_item(self):
        response = self.client.get('/item/get')
        items_data = response.get_json()

        expected_data = {
            "warrior": {
              "name": "Épée de la mort",
              "pts_item": 2,
              "pts": 4
            },
            "mage": {
              "name": "Baton du puissant sorcier",
              "pts_item": 2,
              "pts": 4
            },
            "archer": {
              "name": "Arc de l'arbre divin",
              "pts_item": 2,
              "pts": 4
            }
        }

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(items_data)
        self.assertEqual(items_data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_get_item_id(self):
        itemName = 'mage'

        response = self.client.get(f'/item/get/{itemName}')
        item_data = response.get_json()

        expected_data = {
            "name": "Baton du puissant sorcier",
            "pts_item": 2,
            "pts": 4
        }

        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(item_data)
        self.assertEqual(item_data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_get_item_id_not_found(self):
        itemName = 'magee'

        response = self.client.get(f'/item/get/{itemName}')
        item_data = response.get_json()

        expected_data = {
            "name": "Baton du puissant sorcier",
            "pts_item": 2,
            "pts": 4
        }

        self.assertEqual(response.status_code, 404)
        self.assertIsNotNone(item_data)
        self.assertEqual(item_data, {"message": "Objet non trouvé"})

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_update_item(self):
        itemName = 'warrior'
        payload = {
            "name": "Updated Sword",
            "pts_item": 3,
            "pts": 15
        }

        response = self.client.put(f'/item/upd/{itemName}', json=payload)
        data = response.get_json()

        expected_data = {"message": f"Item {itemName} modifié"}

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(data)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_clean_item(self):
        response = self.client.post('/item/clean')
        data = response.get_json()
        expected_data = {"message": "Items nettoyés"}

        self.assertEqual(response.status_code, 201)
        self.assertEqual(data, expected_data)

    @mock.patch('builtins.open', mock.mock_open(read_data=json.dumps(data_json_item)))
    def test_clean_item(self):
        response = self.client.post('/item/clean')
        cleaned_data = response.get_json()

        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(cleaned_data)
        self.assertEqual(cleaned_data, {"message": "Items nettoyés"})


if __name__ == '__main__':
    unittest.main()
